<?php

/**
 * Home controller class file
 *
 * PHP version 5
 *
 * @category  App
 * @package   Controller
 * @author    Evaldas Kazlauskas <kazlauskas.evaldas@gmail.com>
 * @copyright 2015 Evaldas Kazlauskas
 * @license   http://europa.eu/legislation_summaries/information_society/data_protection/l26053_en.htm Copyright and
 * related rights in the information society
 * @link      http://example.com
 */
namespace Controller;

/**
 * Home controller class
 *
 * @category  App
 * @package   Controller
 * @author    Evaldas Kazlauskas <kazlauskas.evaldas@gmail.com>
 * @copyright 2015 Evaldas Kazlauskas
 * @license   http://europa.eu/legislation_summaries/information_society/data_protection/l26053_en.htm Copyright and
 * related rights in the information society
 * @link      http://example.com
 * @property  \Model\Home @model
 */

class Home extends Controller
{


    /**
     * Main page
     *
     * @return string home page HTML
     */
    function index()
    {
        $toDate   = date('Y-m-d');
        $fromDate = date('Y-m-d', strtotime('-1 month'));
        $isExist  = $this->model->isExistTables();
        if ($isExist === true) {
            $data  = $this->model->dashboardDate($fromDate.' 00:00:00', $toDate.' 23:59:59');
            $chart = json_encode($this->model->chart());
        }

        $this->setVar('to', $toDate);
        $this->setVar('data', $data);
        $this->setVar('from', $fromDate);
        $this->setVar('chart', $chart);
        $this->setVar('isExistTable', $isExist);
        $this->render();

    }//end index()


    /**
     * Load data via ajax
     *
     * @return string
     */
    function ajax()
    {
        $args  = [
                  'dateFrom' => FILTER_SANITIZE_STRING,
                  'dateTo'   => FILTER_SANITIZE_STRING,
                 ];
        $posts = filter_input_array(INPUT_POST, $args);

        $ret = [];
        $ret['is_error']     = false;
        $ret['error_text']   = '';
        $ret['success_text'] = '';

        try {
            $dateFrom = \DateTime::createFromFormat('Y-m-d', $posts['dateFrom']);
            $dateTo   = \DateTime::createFromFormat('Y-m-d', $posts['dateTo']);

            if ($dateFrom === false || $dateTo === false) {
                throw new \Exception("Bad date format");
            }

            $stringFrom          = $dateFrom->format('Y-m-d').' 00:00:00';
            $stringTo            = $dateTo->format('Y-m-d').' 23:59:59';
            $ret['success_text'] = $this->model->dashboardDate($stringFrom, $stringTo);
        } catch (\Exception $error) {
            $ret['is_error']   = true;
            $ret['error_text'] = $error->getMessage();
        }

        return json_encode($ret);

    }//end ajax()


}//end class
