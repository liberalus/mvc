<?php

/**
 * Database data generator controller class file
 *
 * PHP version 5
 *
 * @category  App
 * @package   Controller
 * @author    Evaldas Kazlauskas <kazlauskas.evaldas@gmail.com>
 * @copyright 2015 Evaldas Kazlauskas
 * @license   http://europa.eu/legislation_summaries/information_society/data_protection/l26053_en.htm Copyright and
 * related rights in the information society
 * @link      http://example.com
 */
namespace Controller;
use \Model\Home;
/**
 * Database data generator class
 *
 * @category  App
 * @package   Controller
 * @author    Evaldas Kazlauskas <kazlauskas.evaldas@gmail.com>
 * @copyright 2015 Evaldas Kazlauskas
 * @license   http://europa.eu/legislation_summaries/information_society/data_protection/l26053_en.htm Copyright and
 * related rights in the information society
 * @link      http://example.com
 */

class DBGenerator extends Controller
{


    /**
     * Generate data
     *
     * @return void
     */
    function index()
    {

        $this->model->generate();
        $modelHome = new Home;
        $data      = $modelHome->chart();
        echo json_encode($data);

    }//end index()


}//end class
