<?php

/**
 * Main controller class file
 *
 * PHP version 5
 *
 * @category  App
 * @package   Controller
 * @author    Evaldas Kazlauskas <kazlauskas.evaldas@gmail.com>
 * @copyright 2015 Evaldas Kazlauskas
 * @license   http://europa.eu/legislation_summaries/information_society/data_protection/l26053_en.htm Copyright and
 * related rights in the information society
 * @link      http://example.com
 */
namespace Controller;

/**
 * Main controller class
 *
 * @category  App
 * @package   Controller
 * @author    Evaldas Kazlauskas <kazlauskas.evaldas@gmail.com>
 * @copyright 2015 Evaldas Kazlauskas
 * @license   http://europa.eu/legislation_summaries/information_society/data_protection/l26053_en.htm Copyright and
 * related rights in the information society
 * @link      http://example.com
 */

abstract class Controller
{

    /**
     * Controller's model
     * @var object
     */
    protected $model;

    /**
     * Request
     * @var \Core\Request
     */
    public $request;


    /**
     * Twig
     * @var \Twig_Environment
     */
    public $twig;

    /**
     * Set wars to twig
     * @var array
     */
    private $_vars = [];


    /**
     * Set request object
     *
     * @param \Core\Request $request request object
     *
     * @return void
     */
    public function setRequest(\Core\Request $request)
    {
        $this->request = $request;

    }//end setRequest()


    /**
     * Set model object
     *
     * @param object $model model object
     *
     * @return void
     */
    public function setModel($model)
    {
        $this->model = $model;

    }//end setModel()


    /**
     * Set twig object
     *
     * @param object $twig twig object
     *
     * @return void
     */
    public function setTwig($twig)
    {
        $this->twig = $twig;

    }//end setTwig()


    /**
     * Set var to twig
     *
     * @param string $name variable name
     * @param mixed  $var  variable value
     *
     * @return void
     */
    protected function setVar($name, $var)
    {

        if (trim($name) !== '') {
            $this->_vars[$name] = $var;
        }

    }//end setVar()


    /**
     * Render twig template
     *
     * @param string|bool $templatePath template path; if empty use default path
     *
     * @return void()
     */
    protected function render($templatePath = false)
    {

        $vars = [];

        if (is_array($this->_vars) === true && empty($this->_vars) === false) {
            $vars = $this->_vars;
        }

        $path = $this->request->controller . DS. $this->request->action. '.twig';
        if ($templatePath !== false) {
            $path = $templatePath;
        }

        if (is_file(ROOT_PATH . DS . 'View'. DS . 'Templates' . DS . $path) === true) {
            echo $this->twig->render($path, $vars);
        }

    }//end render()


}//end class
