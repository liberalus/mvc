<?php

/**
 * Dashbord interface file
 *
 * PHP version 5
 *
 * @category  App
 * @package   Interface
 * @author    Evaldas Kazlauskas <kazlauskas.evaldas@gmail.com>
 * @copyright 2015 Evaldas Kazlauskas
 * @license   http://europa.eu/legislation_summaries/information_society/data_protection/l26053_en.htm Copyright and
 * related rights in the information society
 * @link      http://example.com
 */
namespace Model;

/**
 * Dashbord interface class
 *
 * @category  App
 * @package   Interface
 * @author    Evaldas Kazlauskas <kazlauskas.evaldas@gmail.com>
 * @copyright 2015 Evaldas Kazlauskas
 * @license   http://europa.eu/legislation_summaries/information_society/data_protection/l26053_en.htm Copyright and
 * related rights in the information society
 * @link      http://example.com
 */
interface Dashboard
{


    /**
     * Total elements per time period
     *
     * @param bool | string $fromDate from date
     * @param bool | string $toDate   to date
     *
     * @return mixed
     */
    public function total($fromDate = false, $toDate = false);


    /**
     * Top 10 elements per time period
     *
     * @param bool | string $fromDate from date
     * @param bool | string $toDate   to date
     *
     * @return mixed
     */
    public function top10($fromDate = false, $toDate = false);


}//end interface
