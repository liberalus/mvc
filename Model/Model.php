<?php

/**
 * Main model class file
 *
 * PHP version 5
 *
 * @category  App
 * @package   Model
 * @author    Evaldas Kazlauskas <kazlauskas.evaldas@gmail.com>
 * @copyright 2015 Evaldas Kazlauskas
 * @license   http://europa.eu/legislation_summaries/information_society/data_protection/l26053_en.htm Copyright and
 * related rights in the information society
 * @link      http://example.com
 */
namespace Model;

use Core\DataBase;
/**
 * Main controller class
 *
 * @category  App
 * @package   Model
 * @author    Evaldas Kazlauskas <kazlauskas.evaldas@gmail.com>
 * @copyright 2015 Evaldas Kazlauskas
 * @license   http://europa.eu/legislation_summaries/information_society/data_protection/l26053_en.htm Copyright and
 * related rights in the information society
 * @link      http://example.com
 */

abstract class Model
{


    protected  $dataBase = null;


    /**
     * Class construct
     */
    function __construct()
    {
        $this->dataBase = DataBase::getDB();

    }//end __construct()


}//end class
