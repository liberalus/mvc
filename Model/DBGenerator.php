<?php

/**
 * Database generator model class file
 *
 * PHP version 5
 *
 * @category  App
 * @package   Model
 * @author    Evaldas Kazlauskas <kazlauskas.evaldas@gmail.com>
 * @copyright 2015 Evaldas Kazlauskas
 * @license   http://europa.eu/legislation_summaries/information_society/data_protection/l26053_en.htm Copyright and
 * related rights in the information society
 * @link      http://example.com
 */
namespace Model;
use Faker\Factory;

/**
 * Database generator model class
 *
 * @category  App
 * @package   Model
 * @author    Evaldas Kazlauskas <kazlauskas.evaldas@gmail.com>
 * @copyright 2015 Evaldas Kazlauskas
 * @license   http://europa.eu/legislation_summaries/information_society/data_protection/l26053_en.htm Copyright and
 * related rights in the information society
 * @link      http://example.com
 */

class DBGenerator extends Model
{

    /**
     * Faker object
     * @var \Faker\Factory
     */
    private $_faker;


    /**
     * Generate fake mysql data
     *
     * @return void
     */
    public function generate()
    {

        $this->_faker = Factory::create();
        $this->_createTables();
        $this->_generateCustomers();
        $this->_generateOrders();
        $this->_generateOrdersItems();

    }//end generate()


    /**
     * Drop all tables and create news
     *
     * @return void
     */
    private function _createTables()
    {
        $tables['customer']       = "
                    CREATE TABLE IF NOT EXISTS `customer` (
                     `id` int(11) NOT NULL AUTO_INCREMENT,
                     `first_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
                     `last_name`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
                     `email`      varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
                     PRIMARY KEY (`id`)
                     ) ENGINE=InnoDB DEFAULT  CHARACTER SET=utf8 AUTO_INCREMENT=1;
                    ";
        $tables['customer_order'] = "
                    CREATE TABLE IF NOT EXISTS `customer_order` (
                     `id` int(11) NOT NULL AUTO_INCREMENT,
                     `country` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
                     `device` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
                     `purchase_date` datetime DEFAULT NULL,
                     `customer_id` int NOT NULL,
                     `total` double (12,2) DEFAULT 0.0,
                     PRIMARY KEY (`id`),
                     INDEX (`customer_id`)
                     ) ENGINE=InnoDB DEFAULT  CHARACTER SET=utf8 AUTO_INCREMENT=1;
                    ";
        $tables['order_item']     = "
                    CREATE TABLE IF NOT EXISTS `order_item` (
                     `id` int(11) NOT NULL AUTO_INCREMENT,
                     `ean` varchar(13) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
                     `quantity` int DEFAULT 0,
                     `price` double (12,2) DEFAULT 0.0,
                     `order_id` int NOT NULL,
                     `product_id` int NOT NULL,
                     PRIMARY KEY (`id`),
                     INDEX (`order_id`),
                     INDEX (`product_id`)
                     ) ENGINE=InnoDB DEFAULT  CHARACTER SET=utf8 AUTO_INCREMENT=1;
                    ";

        $this->dataBase->rawQuery('SET FOREIGN_KEY_CHECKS=0;');
        foreach ($tables as $table => $query) {
            $this->dataBase->rawQuery('DROP TABLE `'.$table.'`');
            $this->dataBase->rawQuery($query);
        }

        $this->dataBase->rawQuery('SET FOREIGN_KEY_CHECKS=1;');
        $this->dataBase->rawQuery('ALTER TABLE `customer_order` ADD FOREIGN KEY (`customer_id`) REFERENCES customer(`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
        $this->dataBase->rawQuery('ALTER TABLE `order_item` ADD FOREIGN KEY (`order_id`)REFERENCES customer_order(`id`) ON DELETE CASCADE ON UPDATE CASCADE;');

    }//end _createTables()


    /**
     * Generate customers fake data
     *
     * @return void
     */
    private function _generateCustomers()
    {
        $customerTotal = 200;
        for ($index = 0; $index < $customerTotal; $index++) {
            $customer = [];
            $customer['first_name'] = $this->_faker->firstName;
            $customer['last_name']  = $this->_faker->lastName;
            $customer['email']      = $this->_faker->email;
            $this->dataBase->insert('customer', $customer);
        }

    }//end _generateCustomers()


    /**
     * Generate orders fake data
     *
     * @return void
     */
    private function _generateOrders()
    {

        $customers = $this->dataBase->get('customer');
        if (empty($customers) === false) {
            $devices = $this->deviceList();
            foreach ($customers as $customer) {
                $totalOrders = $this->_faker->numberBetween(1, 15);
                for ($index = 0; $index < $totalOrders; $index++) {
                    $date  = $this->_faker->dateTimeThisYear();
                    $order = [];
                    $order['country']       = $this->_faker->country;
                    $order['device']        = $this->_faker->randomElement($devices);
                    $order['purchase_date'] = $date->format('Y-m-d H:i:s');
                    $order['customer_id']   = $customer['id'];
                    $this->dataBase->insert('customer_order', $order);
                }
            }
        }

    }//end _generateOrders()


    /**
     * Devices list
     *
     * @return array
     */
    function deviceList()
    {
        $devices = [
                    'HTC One X',
                    'Samsung Galaxy S II',
                    'Samsung Galaxy Note II',
                    'Samsung Galaxy Note 10.1',
                    'Motorola Xoom',
                    'HTC Desire 500',
                    'Sony Xperia L',
                    'Huawei Ascend Y320',
                    'Toshiba Excite 7c',
                    'Samsung Nexus 10',
                    'Asus Nexus 7 (2nd Gen)',
                    'Samsung Galaxy S4',
                    'Verizon Ellipsis 7',
                    'Samsung Galaxy Note 3',
                    'Motorola Droid RAZR',
                    'Samsung Galaxy S5',
                    'Samsung Galaxy S6',
                    'Samsung Galaxy Tab 2 7.0',
                    'BLU Studio 5.0C HD',
                    'BLU Life View Tab',
                    'Sony Xperia Z3',
                    'Samsung Galaxy Tab 7.7',
                    'HTC One',
                    'Samsung Galaxy Nexus',
                    'Sony Xperia C',
                    'Lenovo S939',
                    'EVGA Tegra Note 7',
                    'Motorola Moto G',
                    'Motorola Moto E',
                    'Samsung Galaxy Note 4',
                    'Motorola Nexus 6',
                    'HTC One S',
                    'Sony Xperia',
                    'HTC Sensation 4G',
                    'Motorola Moto X',
                    'Sony Xperia A',
                    'Samsung Galaxy Tab 4 - white*',
                    'HTC EVO 4G LTE',
                    'LG Optimus G',
                    'HTC Droid DNA',
                    'Sony Xperia E',
                    'LG Optimus G Pro',
                    'LG Nexus 4',
                    'LG Nexus 5',
                    'Samsung Galaxy Tab 3 10.1',
                    'Toshiba Excite Go',
                    'Asus Memo Pad 8',
                    'LG G3',
                    'HTC Desire 510',
                    'NVIDIA SHIELD Tablet',
                    'HTC One M8',
                    'HTC Nexus 9',
                    'LG Spectrum',
                    'HTC Desire X',
                    'Motorola DROID RAZR i',
                    'HTC Butterfly',
                    'HTC EVO 3D',
                    'LG G Pad 7 inch',
                    'iPhone 4',
                    'iPhone 4S',
                    'iPhone 5',
                    'iPhone 5S',
                    'iPhone 5C',
                    'iPhone 6',
                    'iPhone 6 Plus',
                    'iPad',
                    'iPad Air',
                    'iPad Air 2',
                    'iPad mini',
                   ];

        return $devices;

    }//end deviceList()


    /**
     * Generate orders items fake date
     *
     * @return void
     */
    function _generateOrdersItems()
    {
        $maxItems = 100;
        $items    = [];
        // Create all items.
        for ($index = 0; $index < $maxItems; $index++) {
            $item = [];
            $item['product_id'] = $index + 1;
            $item['ean']        = $this->_faker->ean13;
            $item['price']      = $this->_faker->randomFloat(2, 0.5, 1000);
            $items[]            = $item;
        }

        // Create order items.
        $orders = $this->dataBase->get('customer_order');
        if (empty($orders) === false) {
            foreach ($orders as $order) {
                $orderItems = $this->_faker->randomElements($items, $this->_faker->numberBetween(1, 10));
                $orderTotal = 0;
                foreach ($orderItems as $item) {
                    $item['quantity'] = $this->_faker->numberBetween(1, 10);
                    $item['order_id'] = $order['id'];
                    $orderTotal      += $item['quantity'] * $item['price'];
                    $this->dataBase->insert('order_item', $item);
                }

                $this->dataBase->where('id', $order['id']);
                $this->dataBase->update('customer_order', ['total' => $orderTotal]);
            }
        }

    }//end _generateOrdersItems()


}//end class
