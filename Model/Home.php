<?php

/**
 * Home model class file
 *
 * PHP version 5
 *
 * @category  App
 * @package   Model
 * @author    Evaldas Kazlauskas <kazlauskas.evaldas@gmail.com>
 * @copyright 2015 Evaldas Kazlauskas
 * @license   http://europa.eu/legislation_summaries/information_society/data_protection/l26053_en.htm Copyright and
 * related rights in the information society
 * @link      http://example.com
 */
namespace Model;

/**
 * Home model class
 *
 * @category  App
 * @package   Model
 * @author    Evaldas Kazlauskas <kazlauskas.evaldas@gmail.com>
 * @copyright 2015 Evaldas Kazlauskas
 * @license   http://europa.eu/legislation_summaries/information_society/data_protection/l26053_en.htm Copyright and
 * related rights in the information society
 * @link      http://example.com
 */

class Home extends Model
{


    /**
     * Dashboard data
     *
     * @param string $fromDate from date
     * @param string $toDate   to date
     *
     * @return array
     */
    function dashboardDate($fromDate, $toDate)
    {

        $data          = [];
        $modelOrders   = new Orders();
        $modelRevenue  = new Revenue();
        $modelCustomer = new Customer();
        $data['total_orders']    = $modelOrders->total($fromDate, $toDate);
        $data['total_revenue']   = number_format($modelRevenue->total($fromDate, $toDate), 2, ',', ' ');
        $data['total_customer']  = $modelCustomer->total($fromDate, $toDate);
        $data['top_customer']    = $modelCustomer->top10($fromDate, $toDate);
        $data['top_orders']      = $modelOrders->top10($fromDate, $toDate);
        $data['top_orders_item'] = $modelOrders->top10ItemsCount($fromDate, $toDate);
        $data['top_items']       = $modelOrders->top10Items($fromDate, $toDate);

        return $data;

    }//end dashboardDate()


    /**
     * Dashboard chart data
     *
     * @return array
     */
    public function chart()
    {
        $months = 12;
        $dates  = [];
        $dates  = [];
        // Date is 0, order is 1, customer is 2.
        for ($index = $months; $index >= 0; $index--) {
            $dateKey         = date('Y-m', strtotime(date('Y-m').'-01 - '. $index. ' month'));
            $dates[$dateKey] = [
                                0            => $dateKey,
                                1            => 0,
                                2            => 0,
                                '_customers' => []
                               ];
        }

        $results = $this->dataBase->get("customer_order");
        if (empty($results) === false) {
            foreach ($results as $row) {
                $dateKey = date("Y-m", strtotime($row['purchase_date']));
                if (isset($dates[$dateKey]) === true) {
                    $dates[$dateKey][1]++;
                    $dates[$dateKey]['_customers'][] = $row['customer_id'];
                }
            }
        }

        foreach ($dates as &$data) {
            $data[2] = count(array_unique($data['_customers']));
            unset($data['_customers']);
        }

        $dates = array_values($dates);

        return $dates;

    }//end chart()


    /**
     * If exists table
     *
     * @return bool
     */
    function isExistTables()
    {
        $results = $this->dataBase->rawQuery("SHOW TABLES");
        return empty($results) === false ? true : false;

    }//end isExistTables()


}//end class
