<?php

/**
 * Customer orders model class file
 *
 * PHP version 5
 *
 * @category  App
 * @package   Model
 * @author    Evaldas Kazlauskas <kazlauskas.evaldas@gmail.com>
 * @copyright 2015 Evaldas Kazlauskas
 * @license   http://europa.eu/legislation_summaries/information_society/data_protection/l26053_en.htm Copyright and
 * related rights in the information society
 * @link      http://example.com
 */
namespace Model;
use Model\Dashboard;

/**
 * Customer orders model class
 *
 * @category  App
 * @package   Model
 * @author    Evaldas Kazlauskas <kazlauskas.evaldas@gmail.com>
 * @copyright 2015 Evaldas Kazlauskas
 * @license   http://europa.eu/legislation_summaries/information_society/data_protection/l26053_en.htm Copyright and
 * related rights in the information society
 * @link      http://example.com
 */

class Orders extends Model implements Dashboard
{


    /**
     * Total orders per time period
     *
     * @param bool | string $fromDate from date
     * @param bool | string $toDate   to date
     *
     * @return mixed
     */
    public function total($fromDate = false, $toDate = false)
    {

        $this->dataBase->where('purchase_date', [$fromDate, $toDate], 'BETWEEN');
        $count = $this->dataBase->getValue("customer_order", "count(*)");

        return $count;

    }//end total()


    /**
     * Top 10 orders per time period
     *
     * @param bool | string $fromDate from date
     * @param bool | string $toDate   to date
     *
     * @return mixed
     */
    public function top10($fromDate = false, $toDate = false)
    {

        $this->dataBase->where('purchase_date', [$fromDate, $toDate], 'BETWEEN');
        $this->dataBase->orderBy("total", "DESC");
        $results = $this->dataBase->get("customer_order", 10);
        $ret     = [];
        if (empty($results) === false) {
            foreach ($results as $row) {
                $ret[] = 'ID - '.$row['id'].' ('.number_format($row['total'], 2, ",", " "). ' €)';
            }
        }

        return $ret;

    }//end top10()


    /**
     * Top 10 items per time period
     *
     * @param bool | string $fromDate from date
     * @param bool | string $toDate   to date
     *
     * @return mixed
     */
    public function top10Items($fromDate = false, $toDate = false)
    {
        $query   = "SELECT item.product_id, sum(item.quantity) as total, item.order_id FROM customer_order as corder LEFT JOIN order_item as item ON corder.id = item.order_id WHERE purchase_date BETWEEN ? AND ? GROUP BY item.product_id ORDER BY sum(item.quantity) DESC LIMIT 10";
        $results = $this->dataBase->rawQuery($query, [$fromDate, $toDate]);
        $ret     = [];
        if (empty($results) === false) {
            foreach ($results as $row) {
                $ret[] = 'ID - '.$row['product_id'].' ('.$row['total']. ')';
            }
        }

        return $ret;

    }//end top10Items()


    /**
     * Top 10 order by items count per time period
     *
     * @param bool | string $fromDate from date
     * @param bool | string $toDate   to date
     *
     * @return mixed
     */
    public function top10ItemsCount($fromDate = false, $toDate = false)
    {
        $query   = "SELECT count(i.quantity) as total, o.id FROM customer_order as o LEFT JOIN order_item as i ON o.id = i.order_id WHERE purchase_date BETWEEN ? AND ? GROUP BY i.order_id ORDER BY count(i.quantity) DESC LIMIT 10";
        $results = $this->dataBase->rawQuery($query, [$fromDate, $toDate]);
        $ret     = [];
        if (empty($results) === false) {
            foreach ($results as $row) {
                $ret[] = 'ID - '.$row['id'].' ('.$row['total']. ')';
            }
        }

        return $ret;

    }//end top10ItemsCount()


}//end class
