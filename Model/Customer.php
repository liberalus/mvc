<?php

/**
 * Customer model class file
 *
 * PHP version 5
 *
 * @category  App
 * @package   Model
 * @author    Evaldas Kazlauskas <kazlauskas.evaldas@gmail.com>
 * @copyright 2015 Evaldas Kazlauskas
 * @license   http://europa.eu/legislation_summaries/information_society/data_protection/l26053_en.htm Copyright and
 * related rights in the information society
 * @link      http://example.com
 */
namespace Model;
use Model\Dashboard;

/**
 * Customer model class
 *
 * @category  App
 * @package   Model
 * @author    Evaldas Kazlauskas <kazlauskas.evaldas@gmail.com>
 * @copyright 2015 Evaldas Kazlauskas
 * @license   http://europa.eu/legislation_summaries/information_society/data_protection/l26053_en.htm Copyright and
 * related rights in the information society
 * @link      http://example.com
 */

class Customer extends Model implements Dashboard
{


    /**
     * Total customers per time period
     *
     * @param bool | string $fromDate from date
     * @param bool | string $toDate   to date
     *
     * @return integer
     */
    public function total($fromDate = false, $toDate = false)
    {

        $count   = 0;
        $query   = "SELECT count(DISTINCT cu.id) as rev FROM customer  as cu LEFT JOIN customer_order as o ON cu.id = o.customer_id WHERE o.purchase_date BETWEEN ? AND ?";
        $results = $this->dataBase->rawQuery($query, [$fromDate, $toDate]);

        if (isset($results['0']['rev']) === true && $results['0']['rev'] > 0) {
            $count = $results['0']['rev'];
        }

        return $count;

    }//end total()


    /**
     * Top 10 customers per time period
     *
     * @param bool | string $fromDate from date
     * @param bool | string $toDate   to date
     *
     * @return mixed
     */
    public function top10($fromDate = false, $toDate = false)
    {

        $query   = "SELECT cu.id, count(*) as total_order, cu.first_name, cu.last_name FROM customer  as cu LEFT JOIN customer_order as o ON cu.id = o.customer_id WHERE o.purchase_date BETWEEN ? AND ? GROUP BY o.customer_id ORDER BY count(o.customer_id) DESC LIMIT 10;";
        $results = $this->dataBase->rawQuery($query, [$fromDate, $toDate]);
        $ret     = [];
        if (empty($results) === false) {
            foreach ($results as $row) {
                $ret[] = $row['first_name'].' '.$row['last_name'].' ('.$row['total_order']. ')';
            }
        }

        return $ret;

    }//end top10()


}//end class
