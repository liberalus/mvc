<?php

/**
 * Orders revenue model class file
 *
 * PHP version 5
 *
 * @category  App
 * @package   Model
 * @author    Evaldas Kazlauskas <kazlauskas.evaldas@gmail.com>
 * @copyright 2015 Evaldas Kazlauskas
 * @license   http://europa.eu/legislation_summaries/information_society/data_protection/l26053_en.htm Copyright and
 * related rights in the information society
 * @link      http://example.com
 */
namespace Model;
use Model\Dashboard;

/**
 * Orders revenue model class
 *
 * @category  App
 * @package   Model
 * @author    Evaldas Kazlauskas <kazlauskas.evaldas@gmail.com>
 * @copyright 2015 Evaldas Kazlauskas
 * @license   http://europa.eu/legislation_summaries/information_society/data_protection/l26053_en.htm Copyright and
 * related rights in the information society
 * @link      http://example.com
 */

class Revenue extends Model implements Dashboard
{


    /**
     * Total revenue per time period
     *
     * @param bool | string $fromDate from date
     * @param bool | string $toDate   to date
     *
     * @return mixed
     */
    public function total($fromDate = false, $toDate = false)
    {

        $count   = 0;
        $query   = "SELECT sum(total) as rev FROM customer_order WHERE purchase_date BETWEEN ? AND ?";
        $results = $this->dataBase->rawQuery($query, [$fromDate, $toDate]);
        if (isset($results['0']['rev']) === true && $results['0']['rev'] > 0) {
            $count = $results['0']['rev'];
        }

        return $count;

    }//end total()


    /**
     * Top 10 revenue per time period
     *
     * @param bool | string $fromDate from date
     * @param bool | string $toDate   to date
     *
     * @return mixed
     */
    public function top10($fromDate = false, $toDate = false)
    {

        unset($fromDate);
        unset($toDate);
        return [];

    }//end top10()


}//end class
