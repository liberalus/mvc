google.load('visualization', '1.1', {packages: ['line', 'corechart']});
google.setOnLoadCallback(drawChart);

function drawChart() {

    var classicDiv = document.getElementById('chart');
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Month');
    data.addColumn('number', "Orders");
    data.addColumn('number', "Customer");
    data.addRows(chartData);
    var classicOptions = {
        series: {
            0: {targetAxisIndex: 0},
            1: {targetAxisIndex: 1}
        },
        vAxes: {
            0: {title: 'Orders'},
            1: {title: 'Customer'}
        }
    };

    new google.visualization.LineChart(classicDiv).draw(data, classicOptions);
}

$(document).ready(function(){
    $('#datetimepickerFrom, #datetimepickerTo').datetimepicker({
        format: "YYYY-MM-DD",
        disabledTimeIntervals: false,
        showTodayButton: true
    });

    $("#datetimepickerFrom").on("dp.change", function (e) {
        $('#datetimepickerTo').data("DateTimePicker").minDate(e.date);
    });
    $("#datetimepickerTo").on("dp.change", function (e) {
        $('#datetimepickerFrom').data("DateTimePicker").maxDate(e.date);
    });

    $('#datetimepickerFrom, #datetimepickerTo').on("dp.change",datapickerchange);

    $("#generate").click(function(){
        $('.loadajax, #chart').html("<img src='img/loader.gif' />");
        $.ajax({
            url : '/DBGenerator/index',
            dataType: 'json'
        }).done(function (data){
            chartData = data;
            datapickerchange();
            drawChart();

        });
    })

});

function datapickerchange()
{
    $('.loadajax').html("<img src='img/loader.gif' />");
    $.ajax({
        url : '/Home/ajax',
        method: "POST",
        data : {
            dateFrom:  $('#datetimepickerFrom > input').val(),
            dateTo:  $('#datetimepickerTo > input').val()
        },

        dataType: 'json'

    }).done(datePickerDone);
}

function datePickerDone(data)
{

    if (data.is_error == false) {
        $.each(data.success_text, function (key, value) {
            if (typeof value == 'object') {
                text = '';
                for (item in value) {
                    var nr = parseInt(item) +1;
                    text += '<span>' + nr + '. '+ value[item] + '</span>';
                }
                value = text;
            }
            $('#' + key).html(value);
        })
    }
}