<?php

/**
 * Index file initialize app
 *
 * PHP version 5
 *
 * @category  App
 * @package   Main
 * @author    Evaldas Kazlauskas <kazlauskas.evaldas@gmail.com>
 * @copyright 2015 Evaldas Kazlauskas
 * @license   http://europa.eu/legislation_summaries/information_society/data_protection/l26053_en.htm Copyright and
 * related rights in the information society
 * @link      http://example.com
 */

require_once __DIR__ . DIRECTORY_SEPARATOR . 'Config' .DIRECTORY_SEPARATOR . 'constants.php';
require_once ROOT_PATH . DS. 'Vendor' .DS. 'autoload.php';

new \Core\MVC;
