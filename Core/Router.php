<?php

/**
 * Router class file
 *
 * PHP version 5
 *
 * @category  App
 * @package   Core
 * @author    Evaldas Kazlauskas <kazlauskas.evaldas@gmail.com>
 * @copyright 2015 Evaldas Kazlauskas
 * @license   http://europa.eu/legislation_summaries/information_society/data_protection/l26053_en.htm Copyright and
 * related rights in the information society
 * @link      http://example.com
 */
namespace Core;

/**
 * Router class
 *
 * @category  App
 * @package   Core
 * @author    Evaldas Kazlauskas <kazlauskas.evaldas@gmail.com>
 * @copyright 2015 Evaldas Kazlauskas
 * @license   http://europa.eu/legislation_summaries/information_society/data_protection/l26053_en.htm Copyright and
 * related rights in the information society
 * @link      http://example.com
 */

class Router
{


    /**
     * Class construct. Set model and action
     *
     * @param Request $request request object
     *
     * @throws \Exception don't found class or action
     */
    function __construct(Request &$request)
    {
        $isControllerFile   = is_file(ROOT_PATH . DS . 'Controller' . DS .$request->controller . '.php');
        $isControllerAction = method_exists('\\Controller\\'.$request->controller, $request->action);
        if ($isControllerFile === false
            && is_file(ROOT_PATH . DS . 'Controller' . DS .DEFAULT_CONTROLLER . '.php') === true
        ) {
            $request->controller = DEFAULT_CONTROLLER;
        } elseif ($isControllerFile === false) {
            throw new \Exception("Don't found controller");
        }

        if (method_exists('\\Controller\\'.$request->controller, 'index') === true
            &&  $isControllerAction === false
        ) {
            $request->action = 'index';
        } elseif ($isControllerAction === false) {
            throw new \Exception("Don't found action");
        }

    }//end __construct()


}//end class
