<?php

/**
 * DataBase class file
 *
 * PHP version 5
 *
 * @category  App
 * @package   Core
 * @author    Evaldas Kazlauskas <kazlauskas.evaldas@gmail.com>
 * @copyright 2015 Evaldas Kazlauskas
 * @license   http://europa.eu/legislation_summaries/information_society/data_protection/l26053_en.htm Copyright and
 * related rights in the information society
 * @link      http://example.com
 */
namespace Core;

/**
 * DataBase class
 *
 * @category  App
 * @package   Core
 * @author    Evaldas Kazlauskas <kazlauskas.evaldas@gmail.com>
 * @copyright 2015 Evaldas Kazlauskas
 * @license   http://europa.eu/legislation_summaries/information_society/data_protection/l26053_en.htm Copyright and
 * related rights in the information society
 * @link      http://example.com
 */

class DataBase
{

    /**
     * Data base object
     * @var \MysqliDb | bool
     */
    static private $_dataBase = false;


    /**
     * Init databse and return database object
     *
     * @return \MysqliDb
     */
    static function getDB()
    {
        if (self::$_dataBase === false) {
            $dataBaseConfig  = new \Config\DataBase;
            self::$_dataBase = new \MysqliDb(
                $dataBaseConfig->config['host'],
                $dataBaseConfig->config['user'],
                $dataBaseConfig->config['password'],
                $dataBaseConfig->config['database']
            );
        }

        return self::$_dataBase;

    }//end getDB()


}//end class
