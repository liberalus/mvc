<?php

/**
 * Main class file
 *
 * PHP version 5
 *
 * @category  App
 * @package   Core
 * @author    Evaldas Kazlauskas <kazlauskas.evaldas@gmail.com>
 * @copyright 2015 Evaldas Kazlauskas
 * @license   http://europa.eu/legislation_summaries/information_society/data_protection/l26053_en.htm Copyright and
 * related rights in the information society
 * @link      http://example.com
 */
namespace Core;

/**
 * Main class
 *
 * @category  App
 * @package   Core
 * @author    Evaldas Kazlauskas <kazlauskas.evaldas@gmail.com>
 * @copyright 2015 Evaldas Kazlauskas
 * @license   http://europa.eu/legislation_summaries/information_society/data_protection/l26053_en.htm Copyright and
 * related rights in the information society
 * @link      http://example.com
 */

class MVC
{


    /**
     * Class construct. Init MVC proccess.
     */
    function __construct()
    {
        $request = new Request;
        try {
            new Router($request);
            $routerController = '\\Controller\\'.$request->controller;
            $controller       = new $routerController;
            if (is_file(ROOT_PATH . DS . 'Model' . DS .$request->controller . '.php') === true) {
                $modelClass = '\\Model\\'.$request->controller;
                $model      = new $modelClass;
                call_user_func([$controller, 'setModel'], $model);
            }

            \Twig_Autoloader::register();
            $loader = new \Twig_Loader_Filesystem(ROOT_PATH .DS. 'View' . DS. 'Templates');
            $twig   = new \Twig_Environment($loader, ['cache' => false]);
            call_user_func([$controller, 'setTwig'], $twig);
            call_user_func([$controller, 'setRequest'], $request);
            echo call_user_func_array([$controller, $request->action], $request->vars);
        } catch(\Exception $e) {
            echo 'Message: ' .$e->getMessage();
        }

    }//end __construct()


}//end class
