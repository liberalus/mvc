<?php

/**
 * Request class file
 *
 * PHP version 5
 *
 * @category  App
 * @package   Core
 * @author    Evaldas Kazlauskas <kazlauskas.evaldas@gmail.com>
 * @copyright 2015 Evaldas Kazlauskas
 * @license   http://europa.eu/legislation_summaries/information_society/data_protection/l26053_en.htm Copyright and
 * related rights in the information society
 * @link      http://example.com
 */
namespace Core;

/**
 * Request class
 *
 * @category  App
 * @package   Core
 * @author    Evaldas Kazlauskas <kazlauskas.evaldas@gmail.com>
 * @copyright 2015 Evaldas Kazlauskas
 * @license   http://europa.eu/legislation_summaries/information_society/data_protection/l26053_en.htm Copyright and
 * related rights in the information society
 * @link      http://example.com
 */

class Request
{

    /**
     * Request uri
     * @var string
     */
    public $uri;
    public $action;
    public $controller;
    public $vars;
    public $unsafeVars;


    /**
     * Class construct. Set variables
     */
    function __construct()
    {
        $this->uri = $_SERVER['REQUEST_URI'];
        $data      = array_filter(explode("/", $this->uri));
        $data      = array_values($data);
        if (isset($data[0]) === true) {
            $this->controller = filter_var($data[0], FILTER_SANITIZE_STRING);
        }

        if (isset($data[1]) === true) {
            $this->action = filter_var($data[1], FILTER_SANITIZE_STRING);
        }

        $this->unsafeVars = array_slice($data, 2);

        if (empty($this->unsafeVars) === true) {
            $this->unsafeVars = [];
            $this->vars       = [];
        } else {
            foreach ($this->unsafeVars as $var) {
                $this->vars[] = filter_var($var, FILTER_SANITIZE_STRING);
            }
        }

    }//end __construct()


}//end class
