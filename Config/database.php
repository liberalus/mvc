<?php

/**
 * DataBase config parameters
 *
 * PHP version 5
 *
 * @category  App
 * @package   Config
 * @author    Evaldas Kazlauskas <kazlauskas.evaldas@gmail.com>
 * @copyright 2015 Evaldas Kazlauskas
 * @license   http://europa.eu/legislation_summaries/information_society/data_protection/l26053_en.htm Copyright and
 * related rights in the information society
 * @link      http://example.com
 */

namespace Config;

/**
 * DataBase config
 *
 * @category  App
 * @package   Config
 * @author    Evaldas Kazlauskas <kazlauskas.evaldas@gmail.com>
 * @copyright 2015 Evaldas Kazlauskas
 * @license   http://europa.eu/legislation_summaries/information_society/data_protection/l26053_en.htm Copyright and
 * related rights in the information society
 * @link      http://example.com
 */

class DataBase
{

    public $config;


    /**
     * Class construct. Init parameters depend on environment
     */
    function __construct()
    {
        $dataBaseConfig['PROD'] = [
                                   'host'     => 'localhost',
                                   'user'     => 'letsplay_estina',
                                   'password' => 'La9OOuBv',
                                   'database' => 'letsplay_estina',
                                  ];

        $dataBaseConfig['DEV'] = [
                                  'host'     => 'localhost',
                                  'user'     => 'root',
                                  'password' => '',
                                  'database' => 'estina',
                                 ];

        $this->config = $dataBaseConfig[ENV];

    }//end __construct()


}//end class
