<?php

/**
 * File contain app constants
 *
 * PHP version 5
 *
 * @category  App
 * @package   Core
 * @author    Evaldas Kazlauskas <kazlauskas.evaldas@gmail.com>
 * @copyright 2015 Evaldas Kazlauskas
 * @license   http://europa.eu/legislation_summaries/information_society/data_protection/l26053_en.htm Copyright and
 * related rights in the information society
 * @link      http://example.com
 */

define('DS', DIRECTORY_SEPARATOR);
define('ROOT_PATH', realpath(__DIR__. DS. '..'));
define('DEFAULT_CONTROLLER', 'Home');


$env = "PROD";
if (in_array($_SERVER['REMOTE_ADDR'], ['127.0.0.1', "::1"]) === true) {
    $env = "DEV";
}

define('ENV', $env);
